package com.gtw.file.security;

import java.util.Collection;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class LoginUser implements UserDetails {

  @Getter private Long userId;

  @Getter @Setter private String ipAddress;

  private String principalName;
  private String password;
  private Boolean active;
  private Boolean blocked;
  private Collection<? extends GrantedAuthority> authorities;
  @Getter private String realm;

  public LoginUser(
      Long userId,
      String principalName,
      Collection<? extends GrantedAuthority> authorities,
      String realm) {
    this(userId, principalName, null, true, false, authorities, realm);
  }

  public LoginUser(
      Long userId,
      String principalName,
      String password,
      Boolean active,
      Boolean blocked,
      Collection<? extends GrantedAuthority> authorities,
      String realm) {
    this.userId = userId;
    this.principalName = principalName;
    this.password = password;
    this.active = active;
    this.blocked = blocked;
    this.authorities = authorities;
    this.realm = realm;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return authorities;
  }

  @Override
  public String getPassword() {
    return password;
  }

  @Override
  public String getUsername() {
    return principalName;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return !blocked;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return active;
  }

  public String getPrincipalName() {
    return principalName;
  }

  public void setPrincipalName(String principalName) {
    this.principalName = principalName;
  }
}
