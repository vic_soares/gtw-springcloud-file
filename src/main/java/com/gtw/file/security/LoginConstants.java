package com.gtw.file.security;

public class LoginConstants {

  public static final String JWT_TOKEN_PREFIX = "Bearer";

  public static final String ROLE_PREFIX = "Role.";

  public static final String PERMISSION_PREFIX = "Permission.";

}
