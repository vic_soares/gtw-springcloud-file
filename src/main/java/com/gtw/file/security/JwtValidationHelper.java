package com.gtw.file.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import java.util.*;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class JwtValidationHelper {

  @SuppressWarnings("unchecked")
  public LoginUser validateToken(String token) throws AuthenticationException {
    int i = token.lastIndexOf('.');
    String withoutSignature = token.substring(0, i + 1);

    Claims claims = Jwts.parserBuilder().build().parseClaimsJwt(withoutSignature).getBody();
    String realm = claims.get("iss").toString();
    realm = realm.substring(realm.lastIndexOf("/") + 1);

    List roleList = new ArrayList();
    Long userId = claims.get("userId", Long.class);
    String username = claims.get("preferred_username", String.class);

    Set<GrantedAuthority> authorities = new HashSet<>();
    List<String> roles = (List<String>) claims.get("roles", List.class);
    if (CollectionUtils.isNotEmpty(roles)) {
      for (String role : roles) {
        authorities.add(new SimpleGrantedAuthority(LoginConstants.ROLE_PREFIX + role));
      }
    }

    claims
        .get("resource_access", LinkedHashMap.class)
        .values()
        .forEach(o -> roleList.add(((LinkedHashMap) o).get("roles")));

    roleList.forEach(
        r ->
            ((ArrayList) r)
                .forEach(
                    a ->
                        authorities.add(
                            new SimpleGrantedAuthority(LoginConstants.PERMISSION_PREFIX + a))));

    return new LoginUser(userId, username, authorities, realm);
  }
}
