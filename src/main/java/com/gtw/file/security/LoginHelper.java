package com.gtw.file.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class LoginHelper {

  public Authentication forceLogIn(UserDetails user) {
    Authentication authDetails =
        new UsernamePasswordAuthenticationToken(user, user.getPassword(), user.getAuthorities());
    SecurityContextHolder.getContext().setAuthentication(authDetails);
    return authDetails;
  }

  public void logOff() {
    try {
      SecurityContextHolder.getContext().setAuthentication(null);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public <T extends UserDetails> T getLoggedInUser(Class<T> userType) {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication != null) {
      Object principal = authentication.getPrincipal();
      if (principal == null || principal.toString().equals("anonymousUser")) {
        return null;
      } else if (userType.isAssignableFrom(principal.getClass())) {
        return userType.cast(principal);
      }
      throw new IllegalStateException("Invalid user type");
    }
    return null;
  }

  public boolean hasLoggedUser() {
    return getLoggedInUser(UserDetails.class) != null;
  }
}
