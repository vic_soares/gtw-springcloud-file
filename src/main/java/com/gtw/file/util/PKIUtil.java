package com.gtw.file.util;

import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class PKIUtil {

  public static PrivateKey readPrivateKey(String privateKeyText)
      throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
    String keyEncoded =
        privateKeyText
            .replace("-----BEGIN PRIVATE KEY-----", "")
            .replace("-----END PRIVATE KEY-----", "");
    byte[] keyData = Base64.getMimeDecoder().decode(keyEncoded);
    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    return keyFactory.generatePrivate(new PKCS8EncodedKeySpec(keyData));
  }

  public static PublicKey readPublicKey(String publicKeyText)
      throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
    String keyEncoded =
        publicKeyText
            .replace("-----BEGIN PUBLIC KEY-----", "")
            .replace("-----END PUBLIC KEY-----", "");
    byte[] keyData = Base64.getMimeDecoder().decode(keyEncoded);
    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
    return keyFactory.generatePublic(new X509EncodedKeySpec(keyData));
  }
}
