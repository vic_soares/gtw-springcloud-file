package com.gtw.file.model;

public class AccessToken {

  private String sub;
  private String email_verified;
  private String preferred_username;
  private String user_name;

  public String getSub() {
    return sub;
  }

  public void setSub(String sub) {
    this.sub = sub;
  }

  public String getEmail_verified() {
    return email_verified;
  }

  public void setEmail_verified(String email_verified) {
    this.email_verified = email_verified;
  }

  public String getPreferred_username() {
    return preferred_username;
  }

  public void setPreferred_username(String preferred_username) {
    this.preferred_username = preferred_username;
  }

  public String getUser_name() {
    return user_name;
  }

  public void setUser_name(String user_name) {
    this.user_name = user_name;
  }
}
