package com.gtw.file.model.error;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.http.HttpStatus;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
@NoArgsConstructor
public class ErrorMessage {

  @NonNull @JsonIgnore HttpStatus status;

  @NonNull Integer code;

  @NonNull private String message;

  private String details;
}
