package com.gtw.file.model.error;

import org.springframework.http.HttpStatus;

public class CommonErrors {

  public static final ErrorMessage INVALID_PARAMETERS =
      new ErrorMessage(HttpStatus.BAD_REQUEST, 400000, "Invalid Parameters");

  public static final ErrorMessage UNAUTHORIZED =
      new ErrorMessage(HttpStatus.UNAUTHORIZED, 401000, "Not Authorized");

  public static final ErrorMessage FORBIDDEN =
      new ErrorMessage(HttpStatus.FORBIDDEN, 403000, "Access Denied");

  public static final ErrorMessage UNEXPECTED_ERROR =
      new ErrorMessage(HttpStatus.INTERNAL_SERVER_ERROR, 500000, "Unexpected Error");

  public static final ErrorMessage UNEXPECTED_ERROR_FALLBACK =
      new ErrorMessage(
          HttpStatus.INTERNAL_SERVER_ERROR,
          500001,
          "Temporally Unavailable, please contact the Administrator!");
}
