package com.gtw.file;

import javax.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class GtwFileApplication {

  public static void main(String[] args) {
    SpringApplication.run(GtwFileApplication.class, args);
  }

  @PostConstruct
  public void init() {
    log.trace("API Gateway Started!");
  }
}
