package com.gtw.file.exception;

import com.gtw.file.model.error.ErrorMessage;
import lombok.Getter;
import org.springframework.core.NestedRuntimeException;

public class ModelException extends NestedRuntimeException {

  private static final long serialVersionUID = 1L;

  @Getter private ErrorMessage model;

  public ModelException(ErrorMessage model, Throwable cause) {
    super(model.getMessage(), cause);
    this.model = model;
  }

  public ModelException(ErrorMessage model) {
    this(model, null);
  }
}
