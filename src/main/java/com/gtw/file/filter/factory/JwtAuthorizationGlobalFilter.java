package com.gtw.file.filter.factory;

import com.gtw.file.exception.ModelException;
import com.gtw.file.model.AccessToken;
import com.gtw.file.model.error.CommonErrors;
import com.gtw.file.security.JwtValidationHelper;
import com.gtw.file.security.LoginConstants;
import com.gtw.file.security.LoginHelper;
import com.gtw.file.security.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.*;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@Component
public class JwtAuthorizationGlobalFilter implements GlobalFilter, Ordered {

  @Autowired private LoginHelper loginHelper;

  @Autowired private JwtValidationHelper jwtValidationHelper;

  @Value("${keycloak.base-url}")
  private String keycloakUrl;

  private final RestTemplate restTemplate = new RestTemplate();

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
    String token = getAuthorizationToken(exchange.getRequest());
    loginHelper.logOff();

    if (token != null) {
      try {
        LoginUser loginUser = jwtValidationHelper.validateToken(token);
        loginUser.setIpAddress(getClientIpAddress(exchange.getRequest()));
        loginHelper.forceLogIn(loginUser);

        HttpHeaders headers = new HttpHeaders();
        headers.set(
            HttpHeaders.AUTHORIZATION, LoginConstants.JWT_TOKEN_PREFIX.concat(" ").concat(token));
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<AccessToken> response =
            restTemplate.exchange(
                keycloakUrl
                    + "/realms/"
                    + loginUser.getRealm()
                    + "/protocol/openid-connect/userinfo",
                HttpMethod.GET,
                entity,
                AccessToken.class);

        if (response.getStatusCode().value() != HttpStatus.OK.value()) {
          throw new ModelException(CommonErrors.UNAUTHORIZED);
        }
      } catch (Exception e) {
        loginHelper.logOff();
      }
    }

    return chain.filter(exchange);
  }

  @Override
  public int getOrder() {
    return -1;
  }

  private String getAuthorizationToken(ServerHttpRequest request) {
    String authorizationHeader = request.getHeaders().getFirst(HttpHeaders.AUTHORIZATION);
    String token = null;

    if (authorizationHeader != null
        && authorizationHeader.startsWith(LoginConstants.JWT_TOKEN_PREFIX)) {
      token = authorizationHeader.substring(LoginConstants.JWT_TOKEN_PREFIX.length()).trim();
    }

    return token;
  }

  private String getClientIpAddress(ServerHttpRequest request) {
    String ipAddress = request.getHeaders().getFirst("X-Forwarded-For");

    if (ipAddress == null) {
      ipAddress = request.getRemoteAddress().getHostName();
    }

    return ipAddress;
  }
}
