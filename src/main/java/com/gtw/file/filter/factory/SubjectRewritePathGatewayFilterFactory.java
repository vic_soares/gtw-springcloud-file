package com.gtw.file.filter.factory;

import com.gtw.file.exception.ModelException;
import com.gtw.file.model.error.CommonErrors;
import com.gtw.file.security.LoginHelper;
import com.gtw.file.security.LoginUser;
import com.gtw.file.util.JacksonUtil;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.OrderedGatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import reactor.core.publisher.Mono;

import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR;

@Component
public class SubjectRewritePathGatewayFilterFactory extends AbstractGatewayFilterFactory<SubjectRewritePathGatewayFilterFactory.Config> {

    public SubjectRewritePathGatewayFilterFactory() {
        super(SubjectRewritePathGatewayFilterFactory.Config.class);
    }

    @Autowired
    private LoginHelper loginHelper;

    @Validated
    @Data
    public static class Config {
        private String regexp;
    }

    @Override
    public List<String> shortcutFieldOrder() {
        return Collections.singletonList("regexp");
    }

    @Override
    public GatewayFilter apply(SubjectRewritePathGatewayFilterFactory.Config config) {
        return new OrderedGatewayFilter((exchange, chain) -> {
            try {
                if (!loginHelper.hasLoggedUser()) {
                    throw new ModelException(CommonErrors.UNAUTHORIZED);
                }
                LoginUser loginUser = loginHelper.getLoggedInUser(LoginUser.class);
                ServerWebExchangeUtils.addOriginalRequestUrl(exchange, exchange.getRequest().getURI());
                String path = exchange.getRequest().getURI().getRawPath();
                String newPath = path.replaceAll(config.regexp, loginUser.getPrincipalName());
                ServerHttpRequest newRequest = exchange
                        .getRequest()
                        .mutate()
                        .path(newPath)
                        .build();
                exchange.getAttributes().put(GATEWAY_REQUEST_URL_ATTR, newRequest.getURI());
                return chain.filter(exchange.mutate().request(newRequest).build());
            } catch (ModelException e) {
                return handleModelException(e, exchange.getResponse());
            }
        }, 2);
    }

    private Mono<Void> handleModelException(ModelException e, ServerHttpResponse response) {
        response.setStatusCode(e.getModel().getStatus());
        response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
        byte[] bytes = JacksonUtil.toJson(e.getModel()).getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = response.bufferFactory().wrap(bytes);
        return response.writeWith(Mono.just(buffer));
    }

}