package com.gtw.file.filter.factory;

import com.gtw.file.exception.ModelException;
import com.gtw.file.model.error.CommonErrors;
import com.gtw.file.security.LoginHelper;
import com.gtw.file.security.LoginUser;
import com.gtw.file.util.JacksonUtil;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.OrderedGatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class AuthorizationGatewayFilterFactory
    extends AbstractGatewayFilterFactory<AuthorizationGatewayFilterFactory.Config> {

  @Autowired private LoginHelper loginHelper;

  public AuthorizationGatewayFilterFactory() {
    super(Config.class);
  }

  @Data
  public static class Config {
    private List<String> roles = new ArrayList<>();
  }

  @Override
  public List<String> shortcutFieldOrder() {
    return Collections.singletonList("roles");
  }

  @Override
  public ShortcutType shortcutType() {
    return ShortcutType.GATHER_LIST;
  }

  @Override
  public GatewayFilter apply(Config config) {
    return new OrderedGatewayFilter(
        (exchange, chain) -> {
          try {
            if (!loginHelper.hasLoggedUser()) {
              throw new ModelException(CommonErrors.UNAUTHORIZED);
            }
            LoginUser loginUser = loginHelper.getLoggedInUser(LoginUser.class);
            Set<String> loggedUserAuthorities =
                loginUser.getAuthorities().stream()
                    .map(GrantedAuthority::getAuthority)
                    .collect(Collectors.toSet());
            boolean isGranted = config.roles.stream().anyMatch(loggedUserAuthorities::contains);
            if (!isGranted) {
              throw new ModelException(CommonErrors.FORBIDDEN);
            }
            ServerHttpRequest newRequest =
                exchange
                    .getRequest()
                    .mutate()
                    .build();
            return chain.filter(exchange.mutate().request(newRequest).build());
          } catch (ModelException e) {
            return handleModelException(e, exchange.getResponse());
          }
        },
        1);
  }

  private Mono<Void> handleModelException(ModelException e, ServerHttpResponse response) {
    response.setStatusCode(e.getModel().getStatus());
    response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
    byte[] bytes = JacksonUtil.toJson(e.getModel()).getBytes(StandardCharsets.UTF_8);
    DataBuffer buffer = response.bufferFactory().wrap(bytes);
    return response.writeWith(Mono.just(buffer));
  }
}
