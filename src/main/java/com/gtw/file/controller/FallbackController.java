package com.gtw.file.controller;

import com.gtw.file.model.error.CommonErrors;
import com.gtw.file.model.error.ErrorMessage;
import org.springframework.web.bind.annotation.*;

/** Fallback's Controller to Circuit Breaker. */
@RestController
@RequestMapping("/fallback")
public class FallbackController {

  @RequestMapping(
      value = "",
      method = {
        RequestMethod.GET,
        RequestMethod.POST,
        RequestMethod.PUT,
        RequestMethod.PATCH,
        RequestMethod.DELETE
      })
  @ResponseBody
  public ErrorMessage fallback() {
    return CommonErrors.UNEXPECTED_ERROR_FALLBACK;
  }
}
