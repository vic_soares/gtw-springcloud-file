# GTW FILE

Gateteway API File.

## How To Run


1. Change Directory 
```sh
$ cd gtw-springcloud-file/
```


2. Build 
```sh
$ ./gradlew build
```


3. Run 
```sh
$ ./gradlew bootrun
```

4. All the requests are listening on 7070 port. [http://localhost:7070]